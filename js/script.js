function createList(array, parent = document.body) {
    let elementUl = document.createElement("ul");

    array.map(function(item) {
        let listLi = document.createElement("li");

        if (Array.isArray(item)) {
            createList(item, listLi);
        } else {
            listLi.innerText = `${item}`;
        }
        elementUl.append(listLi);

    })
    parent.append(elementUl);

}

createList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);